# Secret Santa (Rust)

Permet de réaliser un secret santa.

Chaque participant présent dans `member_list` se voit attribuer un autre participant de cette même liste.
L'algorithme permet de faire un tirage au hasard en ne formant qu'une seule boucle.

```
member_list = [A, B, C, D];

Tirage 1 boucle :
A -> B
B -> C
C -> D
D -> A

Tirage 2 boucles :
A -> B
B -> A
C -> D
D -> C
```

## Usage

Commencer par mettre à jour la liste `member_list` dans `src/main.rs`.

```rust
    let mut member_list = vec![
        member::Details::new("David", "dope@tech.com"),
        member::Details::new("Claire", "claire@tech.com"),
        ...
    ];
```

Compiler l'application avec `cargo build` puis l'exécuter :

```
$ ./secret_santa
There is 2 members for this christmas.
WORK DONE: You may now send the mail located inside "out"

$ ls out/
david@tech.com
claire@tech.com
```

Il est possible de voir le tirage dans la console avec l'option `-v` :

```
$ ./secret_santa -v
There is 2 members for this christmas.
David offers to Claire
Claire offers to David
WORK DONE: You may now send the mail located inside "out"
```

## Configuration de Mutt avec ProtonMail

```
$ cat ~/.mutt/muttrc
set from = "username@protonmail.com"
set realname = "NAME"

# IMAP settings
set imap_user = "username@protonmail.com"
set imap_pass = ""

# SMTP settings
# proto[s]://[username[:password]@]server[:port][/path]
# La connexion ne doit pas être sécurisée
set smtp_url="smtp://username@protonmail.com@127.0.0.1:1025"
set smtp_pass = "password"
set ssl_starttls=no
set ssl_force_tls=no
```
