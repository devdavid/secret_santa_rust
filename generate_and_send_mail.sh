#!/bin/bash

OUT_DIR=./out

# Re-Create output directory if here yet
rm -rf $OUT_DIR
mkdir $OUT_DIR

# Launch a new pickup
cargo run -- -v -o

echo "-----------------"

# Loop over all files
for file in $OUT_DIR/*
do
    if [ -f $file ]; then
        # Get e-mail from filename
        contactAddr=`basename "$file" .txt`

        if [[ "$contactAddr" =~ ^[+] ]]; then
            echo "WARN $contactAddr is a phone number !"
            continue
        fi

        echo "Sending to $contactAddr..."
		cat "$file" | mutt -s "Secret Santa" $contactAddr
    fi
done

# Cleanup
rm -rf $OUT_DIR

echo Merry Christmas !
