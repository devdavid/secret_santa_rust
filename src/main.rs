/**
 * @desc    Version Rust du Randomizer
 * @date    03/08/23
 * @author  D. DEVANT
 */
use rand::Rng;
use std::env;
use std::fs;
use std::process;
use std::vec;
mod member;

// Constants
const MAIL_MODEL_FILE: &str = "mail.txt";
const MAIL_OUTPUT_DIR: &str = "out";

pub struct Config {
    pub verbose: bool,
    pub use_order: bool,
}

impl Config {
    pub fn parse(args: &[String]) -> Result<Config, &'static str> {
        let mut verbose = false;
        let mut use_order = false;

        // Vérife les arguments
        if args.len() > 3 {
            return Err("Usage: secret_santa [-v]");
        }

        // Extraction de la config
        if args.len() == 3 {
            verbose = args[1] == "-v";
            use_order = args[2] == "-o";
        }

        // Retourne la configuration
        Ok(Config { verbose, use_order })
    }
}

fn main() {
    let args: Vec<String> = env::args().collect();

    // Récupération des arguments
    let config = Config::parse(&args).unwrap_or_else(|err| {
        eprintln!("{err}");
        process::exit(1);
    });

    // Liste nos participants
    let mut member_list = vec![member::Details::new("David", "dope@tech.com")];

    // Charge le modèle du mail
    let mail_model_str = fs::read_to_string(MAIL_MODEL_FILE).expect("\"mail.txt\" not found");

    // Crée le répertoire de sortie
    fs::create_dir_all(MAIL_OUTPUT_DIR).expect("Cannot create output directory");

    println!("There is {} members for this christmas.", member_list.len());

    // Mélange la liste
    if !config.use_order {
        for i in 0..member_list.len() {
            let random = rand::thread_rng().gen_range(0..member_list.len());

            member_list.swap(i, random);
        }
    }

    // Effectue les associations entre membre
    for i in 0..member_list.len() {
        let gifter = member_list.get(i);
        let gifted;

        // On utilise le membre qui suit ou bien le premier si on est rendu à la fin
        if i < (member_list.len() - 1) {
            gifted = member_list.get(i + 1);
        } else {
            gifted = member_list.first();
        }

        // Shadow, Crash en cas de None
        let gifter = gifter.expect("Unable to find a valid gifter");
        let gifted = gifted.expect("Unable to find a valid gifted");

        // Pour le debug
        if config.verbose {
            println!(
                "{} offers to {} ({})",
                gifter.name,
                gifted.name,
                gifted.get_hashed_name()
            );
        }

        // Remplace les champs dans le modèle
        let mail_to_gifter = mail_model_str
            .replace("{gifter}", &gifter.name)
            .replace("{gifted}", &gifted.get_hashed_name());

        if !gifter.email.is_empty() {
            // Crée le chemin du fichier de sortie
            let output_file_path = format!("./{}/{}", MAIL_OUTPUT_DIR, &gifter.email);

            // Ecrit le fichier de sortie
            fs::write(output_file_path, &mail_to_gifter).expect("Unable to write output file");
        }
    }

    // Message de fin
    println!(
        "WORK DONE: You may now send the mail located inside \"{}\"",
        MAIL_OUTPUT_DIR
    );
}
