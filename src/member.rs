#[derive(Debug)]
pub struct Details {
    pub name: String,
    pub email: String,
}

impl Details {
    pub fn new(name: &str, email: &str) -> Self {
        Details {
            name: String::from(name),
            email: String::from(email),
        }
    }

    pub fn get_hashed_name(&self) -> String {
        let mut name: Vec<u8> = self.name.as_bytes().to_vec();

        for i in 0..name.len() {
            name[i] += 1;
        }

        return String::from_utf8(name).expect("UTF-8 required");
    }
}
